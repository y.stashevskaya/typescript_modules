export default function invert<A, B>(obj: A): B {
    const objb = {}
    Object.keys(obj).forEach(key => {
        objb[obj[key]] = isNaN(parseInt(key)) ? key : parseInt(key);
    })

    return objb as B;
}
