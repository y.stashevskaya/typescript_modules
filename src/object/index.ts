import clone from "./clone"
import invert from "./invert"

exports.clone = clone;
exports.invert = invert;