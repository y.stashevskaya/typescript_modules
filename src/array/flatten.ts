type NestedArray<T> = Array<NestedArray<T> | T>;

export default function flatten<T>(elements: NestedArray<T>): Array<T> {
    return elements.flat(12);
}
