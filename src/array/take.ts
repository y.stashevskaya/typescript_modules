export default function take<T>(elements: Array<T>, index: number): Array<T> {
    return elements.slice(0, index);
}
