import flatten from "./flatten"
import take from "./take"

export { flatten, take }